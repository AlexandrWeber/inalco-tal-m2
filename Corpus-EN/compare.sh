#!/bin/bash

cut -f 1 GDN-sample.txt.tt > toks.txt
rm annot-dev*.txt
for dev in AP AV CL EX LW LW2 SW ML; do
	ls GDN-sample.txt.tt.$dev;
	cat GDN-sample.txt.tt.$dev | cut -f 4 > annot-dev$dev.txt
done

paste annot-dev*.txt > annotall.txt
